# dockerfile
#2024-05-24

#BUILD 	docker build -t animelist .
#RUN 	docker run -d -p <host port>:80 -t animelist .
#USE	http://localhost:<host port> 

FROM httpd:2.4

LABEL maintainer="Michelle Banh" email="uyen.banh1@dawsoncollege.qc.ca" modified="2024-05-24"

# DocumentRoot 
WORKDIR /usr/local/apache2/htdocs/

COPY app/* ./

# expose the website port on the container
EXPOSE 80
