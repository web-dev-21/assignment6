## Author info

- Uyen Dinh Michelle Banh
- 420-440-DW Linux II
- my teacher is Adam

## Assignment

- this assignment teaches how to containerize one of my JS applications from the 320 course
- it will be deployed using the two cloud services: Netlify and Azure

## Application

- you search for anime and it will show if it exists

## Instructions

- how to build a dockerfile

- how to run the command from docker hub
