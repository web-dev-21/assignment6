"use strict";

const {useState} = React;

function CreateLi({searchResult}){
  if(searchResult == undefined){
    return null;
  }

  return(
    searchResult.map( (results, index) => <li index={index} className="unselected"> {results.title} </li>)
  );
}

function AnimePage({searchResult, animeIndex}){
  
  if(searchResult == null || animeIndex == null){
    return(
      <div></div>
    );
  }
  else{
    let anime = searchResult[animeIndex];
    
    return(
      <div>
        <h2> {anime.title} </h2>
        
        <div className="anime-info">
          <img src={anime.images.jpg.large_image_url} alt="random anime pic"/>
    
          <p> {anime.synopsis} </p>      
        </div>
      </div>
    );
  }
}

function App(){
  const [search, setSearch] = useState(null);
  const [animeName, setAnimeName] = useState("");
  const [index, setIndex] = useState(null);

  function getAnimeList(){
    fetch(`https://api.jikan.moe/v4/anime?q=${animeName}`)
      .then( response => { 
          console.log( "Response object:", response);
          
          if( !response.ok ) { 
              throw new Error("Not 2xx response", {cause: response});
          }
          return response.json();
      })
      .then( (obj) => {
          console.log("Deserialized response:", obj);
          setSearch(obj.data);
      })
      .catch( err => {
          console.error("Error:", err);
      });
  }

  return(
    <div className="wrapper">
      <header>
        <h1> My Anime List Search </h1>
        
        <form onSubmit={ (e) => {
              e.preventDefault();
              setIndex(null);

              for( let item of document.querySelectorAll("li")){
                item.setAttribute("class", "unselected");
              }

              getAnimeList();
            }}>
            <input type="text" value={animeName} onInput = { (e) =>  setAnimeName(e.target.value)  }/>
            <button type="submit"> search </button>
        </form>

        <ul onClick={ (e) => {
          for( let item of document.querySelectorAll("li")){
            item.setAttribute("class", "unselected");
          }
          setIndex(e.target.getAttribute("index"));
          e.target.setAttribute("class", "selected");
        }}>
          <CreateLi searchResult={search}/>
        </ul>
      </header>

      <main>
        <AnimePage searchResult={search} animeIndex={index}/>        
      </main>
    </div>
    
  );
}

ReactDOM.render(<App />, document.querySelector("#root"));
